#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <cstdio>
#include "graph.hpp"
#include "maze.hpp"

#include "usbd_cdc_vcp.h"

void Node::addEdge(int to,int weight){
	Edge edge;
	edge.to=to;
	edge.cost=weight;
	edges.push_back(edge);
}

void Node::deleteEdge(int to){
	for(Edges::iterator it=edges.begin();it!=edges.end();){
		if((*it).to==to){
			it=edges.erase(it);
			continue;
		}
		it++;
	}
}

void Node::print_node() const{
	printf("index:%d, done:%s, cost:%d, from:%d\nedges:\n",
		index,done?"true":"false",cost,from);
	for(Edges::const_iterator it=edges.begin();it!=edges.end();it++){
		//std::cout<<"cost:"<<(*it).cost<<", to:"<<(*it).to<<std::endl;
	}
}

Graph::Graph(const Maze& maze){
	w=maze.getWidth();
	int cnt=0;
	const MazeData& md=maze.getMazeData();
	for(MazeData::const_iterator it=md.begin();it!=md.end();it++){
		addNode(cnt,-1);
		int cost=3;
		if(!((*it).wall.bits.NORTH))
			addDirectionalEdge(cnt,cnt-w,((*it).wall.bits.SOUTH)?cost:1);
		if(!((*it).wall.bits.EAST))
			addDirectionalEdge(cnt,cnt+1,((*it).wall.bits.WEST)?cost:1);
		if(!((*it).wall.bits.SOUTH))
			addDirectionalEdge(cnt,cnt+w,((*it).wall.bits.NORTH)?cost:1);
		if(!((*it).wall.bits.WEST))
			addDirectionalEdge(cnt,cnt-1,((*it).wall.bits.EAST)?cost:1);
		cnt++;
	}
}

void Graph::addNode(const int index,const int cost){
	Node node(index,cost);
	nodes.push_back(node);
}

void Graph::addEdge(int v,int u,int weight){
	nodes[u].addEdge(v,weight);
	nodes[v].addEdge(u,weight);
}

void Graph::addDirectionalEdge(int from,int to,int weight){
	nodes[from].addEdge(to,weight);
}

void Graph::deleteEdge(int v,int u){
	nodes[u].deleteEdge(v);
	nodes[v].deleteEdge(u);
}

void Graph::print_graph() const{
	int cursor=0;
	for(Nodes::const_iterator it=nodes.begin();it!=nodes.end();it++){
		if((*it).isDone()){
			VCP_Putc('*');
		} else {
			VCP_Putc('o');
		}
		cursor++;
		if(cursor==w){
			VCP_Putc('\n');
			cursor=0;
		}
	}
}

void Graph::dijkstra(int start,int end){
	nodes[start].setCost(0);

	std::priority_queue<Node *> q;
	std::vector<int> in_queue;
	q.push(&nodes[start]);

	while(!q.empty()){
		Node *doneNode=q.top();
		q.pop();
		for(std::vector<int>::iterator it=in_queue.begin();it!=in_queue.end();){
			if((*it)==doneNode->getIndex()){
				it=in_queue.erase(it);
				continue;
			}
			it++;
		}
		doneNode->setDone();
		Edges edges=doneNode->getEdges();
		for(Edges::iterator it=edges.begin();it!=edges.end();it++){
			int to=(*it).to;
			int cost=doneNode->getCost()+(*it).cost;
			if(nodes[to].getCost()<0||cost<nodes[to].getCost()){
				nodes[to].setCost(cost);
				nodes[to].setFrom(doneNode->getIndex());
				if(find(in_queue.begin(),in_queue.end(),to)==in_queue.end()){
					q.push(&nodes[to]);
					in_queue.push_back(to);
				}
			}
		}
	}
}

const Route Graph::getRoute(int start,int end) const{
	int cursor=end;
	Route route;

	while(cursor>=0&&cursor!=start){
		route.push_back(cursor);
		cursor=nodes[cursor].getFrom();
	}
	route.push_back(start);

	return route;
}
