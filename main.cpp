#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"

#include <vector>

#include "usbd_cdc_core.h"
#include "STM32_USB_Device_Library/Core/inc/usbd_usr.h"
#include "usb_conf.h"
#include "usbd_desc.h"

#include "usbd_cdc_vcp.h"

#include "stdio.h"

#include "mouse.hpp"
#include "maze.hpp"
#include "graph.hpp"
#include "viewer.hpp"

using namespace std;
extern uint8_t APP_Rx_Buffer[];

extern uint8_t USB_Connected;

#define STDIN_FILENO 0 /* standard input file descriptor */
#define STDOUT_FILENO 1 /* standard output file descriptor */
#define STDERR_FILENO 2 /* standard error file descriptor */

uint32_t Count = 0x1FFFFF;

const char mazedata[] =
		"9551553ff9551553af92ffc556ffaffaa96aff939553affa8452ffaaa9568552affc53aaaa95693aeffff86c6c2ffaaa9395569553c15286aaafff813ad43aafaaefffac68556aafa85153c556d556c3ae96fabff93ffffaa96d7aaffac53ffa869556affaff8552abafffc556ffaffaaaad515153ffaffaeec55456fc554556";
const Route searchMaze(Mouse* mouse, Viewer* viewer, Maze& realMaze, int maze_w,
		int start, int goal);

void GPIO_Configuration(void);
void Delay(__IO uint32_t nCount);

#ifdef USB_OTG_HS_INTERNAL_DMA_ENABLED
#if defined ( __ICCARM__ ) /*!< IAR Compiler */
#pragma data_alignment=4
#endif
#endif /* USB_OTG_HS_INTERNAL_DMA_ENABLED */

__ALIGN_BEGIN USB_OTG_CORE_HANDLE USB_OTG_dev __ALIGN_END;

int main(void) {
	GPIO_Configuration(); /* GPIO Configuration*/

	setvbuf(stdin, NULL, _IONBF, 0);
	setvbuf(stdout, NULL, _IONBF, 0);
	setvbuf(stderr, NULL, _IONBF, 0);

	vector<uint8_t> pattern;
	pattern.push_back(1);
	pattern.push_back(0);
	pattern.push_back(1);
	pattern.push_back(1);
	pattern.push_back(0);

	USBD_Init(&USB_OTG_dev, USB_OTG_FS_CORE_ID, &USR_desc, &USBD_CDC_cb,
			&USR_cb);

	GPIO_SetBits(GPIOD, GPIO_Pin_12 );
	while (GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0 ) == RESET)
		;
	GPIO_ResetBits(GPIOD, GPIO_Pin_12 );

	int goal = 120;
	int start = 240;

	int type, maze_w = 16, maze_h = 16;

	Maze realMaze(maze_w);
	Maze maze(maze_w);

	for (int i = 0; i < maze_h; i++) {
		for (int j = 0; j < maze_w; j++) {
			CellData cell = { { 0 }, { 0 }, { 0 }, { 0 } };

			char chr = mazedata[i * maze_h + j];

			if (chr >= '0' && chr <= '9') {
				cell.wall.half = chr - '0';
			} else if (chr >= 'a' && chr <= 'f') {
				cell.wall.half = chr + 0xa - 'a';
			} else {
				//std::cerr << "Invalid maze data" << std::endl;
				VCP_Puts("Invalid maze data\n");
				return 1;
			}
			realMaze.addCell(cell);

			cell.wall.half = 0;
			if (j == 0 && i == maze_h - 1)
				cell.wall.bits.EAST = 1;
			if (j == 1 && i == maze_h - 1)
				cell.wall.bits.WEST = 1;
			if (i == 0)
				cell.wall.bits.NORTH = 1;
			if (j == 0)
				cell.wall.bits.WEST = 1;
			if (j == maze_w - 1)
				cell.wall.bits.EAST = 1;
			if (i == maze_h - 1)
				cell.wall.bits.SOUTH = 1;
			if (i * maze_w + j == goal)
				cell.cflag.bits.END = 1;
			if (i * maze_w + j == start)
				cell.cflag.bits.START = 1;
			maze.addCell(cell);
		}
	}

	Viewer *viewer = new Viewer(realMaze);
	viewer->show();

	Mouse *mouse = new Mouse(maze, maze_w);

	searchMaze(mouse, viewer, realMaze, maze_w, start, 255);
	searchMaze(mouse, viewer, realMaze, maze_w, 255, goal);
	searchMaze(mouse, viewer, realMaze, maze_w, goal, 15);
	searchMaze(mouse, viewer, realMaze, maze_w, 15, 0);
	searchMaze(mouse, viewer, realMaze, maze_w, 0, goal);
	searchMaze(mouse, viewer, realMaze, maze_w, goal, start);

	delete mouse;
	delete viewer;

	while (1) /* Toggle LED */
	{
		for (int i = 0; i < pattern.size(); i++) {
			if (pattern[i]) {
				GPIO_SetBits(GPIOD, GPIO_Pin_12 );
			} else {
				GPIO_ResetBits(GPIOD, GPIO_Pin_12 );
			}
			Delay(Count);
		}
	}
}

void GPIO_Configuration(void) {
	//Supply AHB1 Clock
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_GPIOD, ENABLE);

	GPIO_InitTypeDef GPIO_InitStructure;

	/* Configure OB_LED: output push-pull */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOD, &GPIO_InitStructure);

	/* Configure OB_SW: input floating */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
}

const Route searchMaze(Mouse* mouse, Viewer* viewer, Maze& realMaze, int maze_w,
		int start, int goal) {
	int index;
	Route route;

	while ((index = mouse->getCoordIndex()) != goal) {
		Coord coord = mouse->getCoord();

		Graph graph(mouse->getMaze());

		graph.dijkstra(goal, index);
		route = graph.getRoute(goal, index);

		for (Route::iterator it = route.begin() + 1; it != route.end(); it++) {
			//std::cout<<std::endl<<index<<"->"<<(*it)<<std::endl;
			printf("\n%d->%d", index, (*it));

			Direction routeDir = { 0 };

			if ((*it) == index - maze_w) {	//Route: NORTH
				routeDir.half = 1;
			} else if ((*it) == index + 1) {	//Route: EAST
				routeDir.half = 2;
			} else if ((*it) == index + maze_w) {	//Route: SOUTH
				routeDir.half = 4;
			} else if ((*it) == index - 1) {	//Route: WEST
				routeDir.half = 8;
			} else {
				VCP_Puts("wrong path. break.\n");
				return Route();
			}

			mouse->senseAndSetWall(realMaze);

			//std::cout<<index<<":";
			//std::cout<<std::endl;
			//std::cout<<"route:"<<routeDir.half<<"front:"<<mouse->transDirection(Maze::DirFront).half<<std::endl;
			printf("route:%d,front:%d\n", routeDir.half,
					mouse->transDirection(Maze::DirFront).half);

			viewer->append(route);
			viewer->append(mouse->getMaze());
			viewer->append(mouse->getCoord());
			viewer->show();

			if (routeDir.half == mouse->transDirection(Maze::DirFront).half) {
				//std::cout<<"route:front"<<std::endl;
				VCP_Puts("route:front\n");
				if (mouse->senseFront(realMaze)) {
					VCP_Puts("sensor detected:front\n");
					break;
				} else {
					mouse->moveForward();
					Delay(0x1FFFF);
				}
			} else if (routeDir.half
					== mouse->transDirection(Maze::DirRight).half) {
				//std::cout<<"route:right"<<std::endl;
				VCP_Puts("route:right\n");
				if (mouse->senseRight(realMaze)) {
					//std::cout<<"sensor detected:right"<<std::endl;
					VCP_Puts("sensor detected:front\n");
					break;
				} else {
					mouse->turnRight();
					Delay(0x1FFFF);
					mouse->moveForward();
					Delay(0x1FFFF);
				}
			} else if (routeDir.half
					== mouse->transDirection(Maze::DirLeft).half) {
				//std::cout<<"route:left"<<std::endl;
				VCP_Puts("route:left\n");
				if (mouse->senseLeft(realMaze)) {
					//std::cout<<"sensor detected:left"<<std::endl;
					VCP_Puts("sensor detected:left\n");
					break;
				} else {
					mouse->turnLeft();
					Delay(0x1FFFF);
					mouse->moveForward();
					Delay(0x1FFFF);
				}
			} else {
				//std::cout<<"route:back"<<std::endl;
				VCP_Puts("route:back\n");
				mouse->turnRight();
				Delay(0x1FFFF);
				mouse->turnRight();
				Delay(0x1FFFF);
				mouse->moveForward();
				Delay(0x1FFFF);
			}
			index = mouse->getCoordIndex();
			coord = mouse->getCoord();
			Delay(0x1FFFFF);
		}
	}
	return route;
}

void Delay(__IO uint32_t nCount) {
	for (; nCount != 0; nCount--)
		;
}
