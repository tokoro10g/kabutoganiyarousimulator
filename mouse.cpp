#include <iostream>
#include <vector>
#include "mouse.hpp"

const long Mouse::STEP_TIME;
const char* Mouse::dirchr=" ^> v   <";

Mouse::Mouse(const Maze& newmaze,int width){
	w=width;
	c.dir.half=1;
	c.x=0;
	c.y=w-1;
	maze=new Maze(newmaze);
}

void Mouse::senseAndSetWall(const Maze& realMaze){
	if(senseRight(realMaze)){
		//std::cout<<"right,";
		setWall(transDirection(Maze::DirRight));
	}
	if(senseLeft(realMaze)){
		//std::cout<<"left,";
		setWall(transDirection(Maze::DirLeft));
	}
	if(senseFront(realMaze)){
		//std::cout<<"front,";
		setWall(transDirection(Maze::DirFront));
	}
}

bool Mouse::senseFront(const Maze& realMaze) const{
	Direction dir={1};
	return realMaze.getMazeData()[c.y*w+c.x].wall.half&transDirection(dir).half;
}
bool Mouse::senseRight(const Maze& realMaze) const{
	Direction dir={2};
	return realMaze.getMazeData()[c.y*w+c.x].wall.half&transDirection(dir).half;
}
bool Mouse::senseLeft(const Maze& realMaze) const{
	Direction dir={8};
	return realMaze.getMazeData()[c.y*w+c.x].wall.half&transDirection(dir).half;
}
bool Mouse::moveForward(){
	c.x+=!!(c.dir.bits.EAST)-!!(c.dir.bits.WEST);
	c.y+=!!(c.dir.bits.SOUTH)-!!(c.dir.bits.NORTH);
	return true;
}
void Mouse::turnRight(){
	c.dir.half=(c.dir.half<<1)&0xF;
	if(c.dir.half==0) c.dir.half=0x1;
}
void Mouse::turnLeft(){
	c.dir.half=(c.dir.half>>1)&0xF;
	if(c.dir.half==0) c.dir.half=0x8;
}

void Mouse::setMaze(const Maze& newmaze){
	delete maze;
	maze=new Maze(newmaze);
}
void Mouse::clearMaze(){
	int width=maze->getWidth();
	delete maze;
	maze=new Maze(width);
}

void Mouse::setWall(Direction dir){
	maze->setWall(c,dir);
}

Direction Mouse::transDirection(const Direction local) const{
	Direction global=c.dir;
	Direction dir={0};
	if(local.bits.NORTH){
		return global;
	} else if(local.bits.EAST){
		dir.half=(((global.half&0x8)>>3)|(global.half<<1))&0xF;
		return dir;
	} else if(local.bits.WEST){
		dir.half=(((global.half&0x1)<<3)|(global.half>>1))&0xF;
		return dir;
	} else {
		//std::cout<<"ouch"<<std::endl;
		return dir;
	}
}
