SHELL = cmd.exe
TARGET_ARCH   = -mcpu=cortex-m4 -mthumb
INCLUDE_DIRS  = -I . \
				-I ../../Libraries \
				-I ../../Libraries/STM32F4xx_StdPeriph_Driver/inc \
				-I ../../Libraries/STM32_USB_Device_Library/Class/cdc/inc \
				-I ../../Libraries/STM32_USB_Device_Library/Core/inc \
				-I ../../Libraries/STM32_USB_OTG_Driver/inc \
				-I ../../Libraries/CMSIS/Device/ST/STM32F4xx/include \
				-I ../../Libraries/CMSIS/Include \
				-I $(TOOLDIR)../arm-none-eabi/include \
				-I $(TOOLDIR)../arm-none-eabi/include/c++/4.6.2
STARTUP_DIR = ../../Libraries/CMSIS/Device/ST/STM32F4xx/Source/Templates/gcc_ride7/
BOARD_OPTS = -DHSE_VALUE=((uint32_t)8000000) -DSTM32F4XX -DUSE_USB_OTG_FS
FIRMWARE_OPTS = -DUSE_STDPERIPH_DRIVER
COMPILE_OPTS  = -Os -g3 -fpermissive -ffunction-sections -fdata-sections -fsigned-char -fno-rtti -fno-exceptions -Wall -fmessage-length=0 $(INCLUDE_DIRS) $(BOARD_OPTS) $(FIRMWARE_OPTS)

TOOLDIR = ../../yagarto/bin/
CC      = $(TOOLDIR)arm-none-eabi-g++
CXX		= $(CC)
AS      = $(CC)
LD      = $(CC)
AR      = $(TOOLDIR)arm-none-eabi-ar
OBJCOPY = $(TOOLDIR)arm-none-eabi-objcopy
CFLAGS  = $(COMPILE_OPTS)
CXXFLAGS= $(COMPILE_OPTS)
ASFLAGS = -x assembler-with-cpp -c $(TARGET_ARCH) $(COMPILE_OPTS) 
LDFLAGS = -Wl,--gc-sections,-Map=bin\main.map,-cref -T stm32_flash.ld $(INCLUDE_DIRS) -lstdc++ -L $(TOOLDIR)../arm-none-eabi/lib/thumb -L ../../Libraries -nostartfiles -static

all: libstm32f4xx libstm32usb startup bin\main.hex

# main.o is compiled by suffix rule automatucally
bin\main.hex: $(patsubst %.c,%.o,$(wildcard *.c)) $(patsubst %.cpp,%.o,$(wildcard *.cpp)) $(STARTUP_DIR)startup_stm32f4xx.o ../../Libraries/libstm32f4xx.a ../../Libraries/libstm32usb.a 
	$(LD) $(LDFLAGS) $(TARGET_ARCH) $^ -o bin\main.elf 
	$(OBJCOPY) -O ihex bin\main.elf bin\main.hex

# many of xxx.o are compiled by suffix rule automatically
LIB_OBJS = $(sort \
 $(patsubst %.c,%.o,$(wildcard ../../Libraries/STM32F4xx_StdPeriph_Driver/src/*.c)))
 
LIB_OBJS_USB = $(sort \
 $(patsubst %.c,%.o,$(wildcard ../../Libraries/STM32_USB_Device_Library/Class/cdc/src/*.c))\
 $(patsubst %.c,%.o,$(wildcard ../../Libraries/STM32_USB_Device_Library/Core/src/*.c))\
 $(patsubst %.c,%.o,$(wildcard ../../Libraries/STM32_USB_OTG_Driver/src/*.c)))

libstm32f4xx: $(LIB_OBJS)
	$(AR) cr ../../Libraries/libstm32f4xx.a $(LIB_OBJS)

libstm32usb: $(LIB_OBJS_USB)
	$(AR) cr ../../Libraries/libstm32usb.a $(LIB_OBJS_USB)
	
startup:
	$(AS) -o $(STARTUP_DIR)/startup_stm32f4xx.o $(ASFLAGS) $(STARTUP_DIR)startup_stm32f4xx.s

$(LIB_OBJS): \
 $(wildcard ../../Libraries/STM32F4xx_StdPeriph_Driver/inc/*.h) \
 $(wildcard ../../Libraries/STM32F4xx_StdPeriph_Driver/src/*.c) \
 makefile

$(LIB_OBJS_USB): \
 $(wildcard ../../Libraries/STM32_USB_Device_Library/Class/cdc/inc/*.h) \
 $(wildcard ../../Libraries/STM32_USB_Device_Library/Class/cdc/src/*.c) \
 $(wildcard ../../Libraries/STM32_USB_Device_Library/Core/inc/*.h) \
 $(wildcard ../../Libraries/STM32_USB_Device_Library/Core/src/*.c) \
 $(wildcard ../../Libraries/STM32_USB_OTG_Driver/inc/*.h) \
 $(wildcard ../../Libraries/STM32_USB_OTG_Driver/src/*.c) \
 makefile

clean:
	del /f /q *.o *.s bin\*